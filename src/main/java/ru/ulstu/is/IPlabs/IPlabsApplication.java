package ru.ulstu.is.IPlabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@RestController
@SpringBootApplication
public class IPlabsApplication {

	public static void main(String[] args) {
		SpringApplication.run(IPlabsApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s!", name);
	}

	@GetMapping("/math")
	public String SquareRoot(@RequestParam(value = "number", defaultValue = "10") int number){
		return String.format("The square is: %s, root is: %s",number*number,Math.pow(number,0.5));
	}
}
